AC_INIT([spasm],[1.2],[charles.bouillaguet@lip6.fr])
AC_PREREQ([2.69])
#AC_CANONICAL_HOST
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_MACRO_DIR([m4])
LT_INIT

AM_INIT_AUTOMAKE([1.8 check-news no-dependencies foreign silent-rules subdir-objects -Wall -Wportability])
AC_CONFIG_HEADERS([src/config.h])
AM_MAINTAINER_MODE

AC_LANG([C])

# user-definable options

# Checks for programs.
AC_PROG_CC
AC_PROG_CXX
AC_PROG_CC_C99
AC_PROG_MAKE_SET
AC_PROG_AWK     # for TAP
AC_PROG_LIBTOOL
AC_PROG_INSTALL
AM_PROG_CC_C_O

AC_REQUIRE_AUX_FILE([tap-driver.sh])

# Checks for header files.
#AC_HEADER_STDC

# Checks for typedefs, structures, and compiler characteristics.
#AC_FUNC_MALLOC

DEFAULT_CHECKING_PATH="/usr /usr/local /opt/local"
SPASM_CHECK_OMP
SPASM_CHECK_METIS
SPASM_CHECK_GIVARO
SPASM_CHECK_FFLAS_FFPACK
SPASM_CHECK_LINBOX
SPASM_CHECK_LEMON

# AC_CANONICAL_HOST is needed to access the 'host_os' variable    
AC_CANONICAL_HOST

# Mathematica
# Detect the target system
case "${host_os}" in
	linux*)
		LIB_SUFFIX=so
		MATH_CMD=math
		AC_CHECK_PROG(MMA_CHECK,math,yes)
		AS_IF([test x"$MMA_CHECK" != x"yes"], [AC_MSG_ERROR([Mathematica not found.])])
		MMA_CFLAGS=-I`${MATH_CMD} -script ${ac_pwd}/../includeDir.m`
		AC_SUBST(MMA_SYSTEM_ID, `${MATH_CMD} -script ${ac_pwd}/../systemID.m`)
        ;;
	darwin*)
		LIB_SUFFIX=dylib
		MATH_CMD=/Applications/Mathematica.app/Contents/MacOS/MathKernel
		AC_CHECK_PROG(MMA_CHECK,MathKernel,yes,no,/Applications/Mathematica.app/Contents/MacOS)
		AS_IF([test x"$MMA_CHECK" != x"yes"], [AC_MSG_ERROR([Mathematica not found.])])
		MMA_CFLAGS=-I`${MATH_CMD} -script ${ac_pwd}/../includeDir.m`
		AC_SUBST(MMA_SYSTEM_ID, `${MATH_CMD} -script ${ac_pwd}/../systemID.m`)
		;;
	*)
		AC_MSG_ERROR(["OS $host_os is not supported"])
		;;
esac
# if mathematica isn't recognized or doesn't produce the correct MMA_... flags, add them here by hand and
# comment out the code above and instead uncomment and edit those lines:
#AC_SUBST(MMA_INCLUDE_DIR, <output of $InstallationDirectory<>"/SystemFiles/IncludeFiles/C">)
#AC_SUBST(MMA_SYSTEM_ID, <output of $SystemID>)

AC_SUBST(LIB_SUFFIX)

CFLAGS="$CFLAGS $OMPFLAGS $MMA_CFLAGS -Wall -Wextra -fPIC"
CXXFLAGS="$CXXFLAGS $OMPFLAGS $FFLAS_FFPACK_CXXFLAGS $GIVARO_CXXFLAGS -Wall -Wextra -fPIC"
LDFLAGS="$LDFLAGS $FFLAS_FFPACK_LIBS $GIVARO_LIBS"

AC_SUBST(CFLAGS)
AC_SUBST(CXXFLAGS)
AC_SUBST(LDFLAGS)

AC_CONFIG_FILES([Makefile src/Makefile test/Makefile bench/Makefile])
AC_OUTPUT
