#include "libraryLink.h"

#include <stdio.h>

EXTERN_C DLLEXPORT int LL_rref(WolframLibraryData libData_, mint argc_, MArgument* args_,
							   MArgument res_) {
	if (!(argc_ == 1 || argc_ == 2)) {
		return LIBRARY_FUNCTION_ERROR;
	}
	
	//get input SparseArray
	MSparseArray arg = MArgument_getMSparseArray(args_[0]);
	//get the prime number
	int64_t prime = 3037000493;
	if (argc_ == 2) {
		prime = MArgument_getInteger(args_[1]);
	}
	
	spasm* A = fromMathematica(&arg, libData_, prime);

	//do the reduction
	spasm* V = rref_gplu(A);
	spasm_csr_free(A);
	spasm* U = rref(V);
	spasm_csr_free(V);

	MSparseArray ret = toMathematica(U, libData_, prime);
	
	//free spaSM stuff
	spasm_csr_free(U);

	//return to mathematica
	MArgument_setMSparseArray(res_, ret);

	return LIBRARY_NO_ERROR;
}

EXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_) {
	return LIBRARY_NO_ERROR;
}

EXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_) {
}

spasm* rref_gplu(spasm* a_) {
	spasm_lu *LU = spasm_LU(a_, SPASM_IDENTITY_PERMUTATION, 1);
	spasm *U = spasm_transpose(LU->L, 1);

	spasm_free_LU(LU);
	
	spasm_make_pivots_unitary(U, SPASM_IDENTITY_PERMUTATION, U->n);

	return U;
}

spasm* rref(spasm* a_) {
	int64_t n = a_->n;
	int64_t m = a_->m;
	int64_t *p = spasm_malloc(n * sizeof(int64_t));
	int64_t *qinv = spasm_malloc(m * sizeof(int64_t));
	int64_t *Up = a_->p;
	int64_t *Uj = a_->j;

	/* check that U is permuted lower-triangular*/
	for (int64_t j = 0; j < m; j++) {
		qinv[j] = -1;
	}
	
	for (int64_t i = 0; i < n; i++) {		
		for (int64_t px = Up[i]; px < Up[i + 1]; px++)
			if (qinv[Uj[px]] != -1) {
				fprintf(stderr, "the input matrix is not (permuted) upper-triangular.\n");
				exit(1);
			}
		qinv[Uj[Up[i]]] = i;
	}

	/* build the RREF. This code is similar to src/spasm_schur.c ---> in bad need of factorization */
	spasm *R = spasm_csr_alloc(n, m, spasm_nnz(a_), a_->prime, SPASM_WITH_NUMERICAL_VALUES);
	int64_t *Rp = R->p;
	int64_t *Rj = R->j;
	int64_t *Rx = R->x;	
	int64_t rnz = 0;
	int64_t writing = 0;
	int64_t k = 0;

	#pragma omp parallel
	{
		spasm_GFp *x = spasm_malloc(m * sizeof(*x));
		int64_t *xj = spasm_malloc(3 * m * sizeof(int64_t));
		spasm_vector_zero(xj, 3 * m);
		int64_t *qinv_local = spasm_malloc(m * sizeof(int64_t));

		for (int64_t j = 0; j < m; j++)
			qinv_local[j] = qinv[j];

		#pragma omp for schedule(dynamic, 10)
	  	for (int64_t i = 0; i < n; i++) {
	  		int64_t j = Uj[Up[i]];
	  		assert(qinv_local[j] == i);
	  		qinv_local[j] = -1;
	  		int64_t top = spasm_sparse_forward_solve(a_, a_, i, xj, x, qinv_local);
	  		
			/* ensure R has the "pivot first" property */
			for (int64_t px = top + 1; px < m; px++)
				if (xj[px] == j) {
					xj[px] = xj[top];
					xj[top] = j;
					break;
				}
			assert(xj[top] == j);

	  		/* count the NZ in the new row */
	  		int64_t row_nz = 0;
			for (int64_t px = top; px < m; px++) {
				j = xj[px];
				if ((qinv_local[j] < 0) && (x[j] != 0))
					row_nz++;
			}

			int64_t row_k, row_px;
			#pragma omp critical(rref)
			{
				/* not enough room in R ? realloc twice the size */
				if (rnz + m > R->nzmax) {
					/* wait until other threads stop writing into R */
					#pragma omp flush(writing)
					while (writing > 0) {
						#pragma omp flush(writing)
					}
					spasm_csr_realloc(R, 2 * R->nzmax + m);
					Rj = R->j;
					Rx = R->x;
				}
				/* save row k */
				row_k = k++;
				row_px = rnz;
				rnz += row_nz;
				/* this thread will write into R */
				#pragma omp atomic update
				writing++;
			}

			/* write the new row in R */
			Rp[row_k] = row_px;
			for (int64_t px = top; px < m; px++) {
				int64_t j = xj[px];
				if (qinv_local[j] < 0 && x[j] != 0) {
					Rj[row_px] = xj[px];
					Rx[row_px] = x[j];
					row_px++;
				}
			}

			/* we're done writing */
			#pragma omp atomic update
			writing--;

		} /* for */
	  	free(x);
		free(xj);
		free(qinv_local);
	} /* parallel section */
	Rp[n] = rnz;

	/* build p to order the rows */
	for (int64_t j = 0; j < m; j++)
		qinv[j] = -1;
	for (int64_t i = 0; i < n; i++)
		qinv[Rj[Rp[i]]] = i;
	k = 0;
	for (int64_t j = 0; j < m; j++)
		if (qinv[j] >= 0)
			p[k++] = qinv[j];
	assert(k == n);

	

	spasm *S = spasm_permute(R, p, SPASM_IDENTITY_PERMUTATION, SPASM_WITH_NUMERICAL_VALUES);
	free(p);
	spasm_csr_free(R);
	free(qinv);

	return S;
}

spasm* fromMathematica(MSparseArray* arg_, WolframLibraryData libData_, const int64_t prime_) {
	//get pointer to sparse functions
	WolframSparseLibrary_Functions sparseFuns = libData_->sparseLibraryFunctions;
	
	//initialize MTensors for positions and values
	MTensor pos = 0;
	(*(sparseFuns->MSparseArray_getExplicitPositions))(*arg_, &pos);
	MTensor* values = (*(sparseFuns->MSparseArray_getExplicitValues))(*arg_);

	//get necessary information and c arrays to access data efficiently
	mint const* dims = (*(sparseFuns->MSparseArray_getDimensions))(*arg_);
	mint len = libData_->MTensor_getFlattenedLength(*values);
	mint* posArray = libData_->MTensor_getIntegerData(pos);
	mint* valuesArray = libData_->MTensor_getIntegerData(*values);

	//TODO: directly use the crs format
	//construct the spaSM matrix
	spasm_triplet *T = spasm_triplet_alloc(dims[0], dims[1], 1, prime_, prime_ != -1);
	mint* itV = valuesArray;
	mint* itP = posArray;
	for (mint i = 0; i != len; ++i) {
		spasm_add_entry(T, *(itP) - 1, *(itP + 1) - 1, *(itV++));
		itP += 2;
	}

	//free pos MTensor
	libData_->MTensor_free(pos);

	spasm_triplet_transpose(T);
	spasm *A = spasm_compress(T);
	spasm_triplet_free(T);

	return A;
}

MSparseArray toMathematica(const spasm* U_, WolframLibraryData libData_, const int64_t prime_) {
	//get pointer to sparse functions
	WolframSparseLibrary_Functions sparseFuns = libData_->sparseLibraryFunctions;
	
	MTensor posRet, valuesRet;
	//count number of entries (TODO: better?)
	int64_t* Up = U_->p;
	int64_t n = U_->n;
	mint counter = 0;
	for (int64_t i = 0; i != n; ++i) {
		for (int64_t px = Up[i]; px != Up[i + 1]; ++px) {
			++counter;
		}
	}
	const mint dimsPos[] = {counter,2};
	const mint dimsValues[] = {counter};
	libData_->MTensor_new(MType_Integer, 2, dimsPos, &posRet);
	libData_->MTensor_new(MType_Integer, 1, dimsValues, &valuesRet);
	mint posCounter[] = {1,1}, valuesCounter = 1;
	int64_t* Uj = U_->j;
	spasm_GFp* Ux = U_->x;
	//read out the positions and values
	for (int64_t i = 0; i != n; ++i) {
		for (int64_t px = Up[i]; px != Up[i + 1]; ++px) {
			spasm_GFp x = (Ux != NULL) ? Ux[px] : 1;
			x = (x > prime_ / 2) ? x - prime_ : x;
			libData_->MTensor_setInteger(posRet, posCounter, i + 1);
			++posCounter[1];
			libData_->MTensor_setInteger(posRet, posCounter, Uj[px] + 1);
			++posCounter[0];
			posCounter[1] = 1;
			libData_->MTensor_setInteger(valuesRet, &valuesCounter, x);
			++valuesCounter;
		}
	}

	//construct return SparseArray
	MSparseArray ret = 0;
	mint dimsRet[] = {2};
	MTensor dimsRetTens;
	libData_->MTensor_new(MType_Integer, 1, dimsRet, &dimsRetTens);
	dimsRet[0] = 1;
	libData_->MTensor_setInteger(dimsRetTens, dimsRet, U_->n);
	dimsRet[0] = 2;
	libData_->MTensor_setInteger(dimsRetTens, dimsRet, U_->m);
	(*(sparseFuns->MSparseArray_fromExplicitPositions))(posRet, valuesRet, dimsRetTens, 0, &ret);

	//free mathematica stuff
	libData_->MTensor_free(posRet);
	libData_->MTensor_free(valuesRet);
	libData_->MTensor_free(dimsRetTens);

	return ret;
}
