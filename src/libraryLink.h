#ifndef LIBRARYLINK_H
#define LIBRARYLINK_H

#include "WolframLibrary.h"
#include "WolframSparseLibrary.h"
#include "spasm.h"

EXTERN_C DLLEXPORT int WolframLibrary_getVersion() {
	return WolframLibraryVersion;
}

EXTERN_C DLLEXPORT int LL_rref(WolframLibraryData libData_, mint argc_, MArgument* args_, MArgument res_);

EXTERN_C DLLEXPORT int WolframLibrary_initialize(WolframLibraryData libData_);

EXTERN_C DLLEXPORT void WolframLibrary_uninitialize(WolframLibraryData libData_);

spasm* rref_gplu(spasm* a_);

spasm* rref(spasm* a_);

spasm* fromMathematica(MSparseArray* arg_, WolframLibraryData libData_, const int64_t prime_);

MSparseArray toMathematica(const spasm*, WolframLibraryData libData_, const int64_t prime_);

#endif /*LIBRARYLINK_H*/
